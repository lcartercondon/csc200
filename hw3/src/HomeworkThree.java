import java.util.Scanner;


/**
 * @author Liam Carter-Condon
 * @email liam.carter.condon@gmail.com
 * @email lmc2713@email.vccs.edu
 */

public class HomeworkThree {

  /**
   * Write a program to convert the user input baed on the following specs. The user input has the following format:
   *    [variable-length][a space][A|B|H|M]
   * 1. If the input is a number followed by the character "A" output the ASCII representation of (number -1), (number), and (number +1).
   *    a. Assume the number to be a 3-digit decimal number.
   *    b. If the above assumption is not correct, output "PASS".
   * 2. If the input is a number followed by the character "B" output the decimal equivalent. 
   *    a. Assume the numeric to be a 4-digit binary number.
   *    b. If the above assumption is not correct, output "PASS".
   * 3. If the input is a number followed by the character "H" output the binary equivalent.
   *    a. Assume the number to be a 2-digit hexadecmial number.
   *    b. If the above assumption is not correct, output "PASS".
   * 4. If the input is a number followed by the character "M" output the multiplication of all digits in decimal.
   *    a. Assume the number to be a one to three digit octal.
   *    b. If the above assumption is not correct, output "PASS".
   * 5. If none of the above conditions is met, output "Goodbye."
   */

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    String numeric = in.next();
    String format = in.next();
    char fmt = (format.length() == 1) ? format.charAt(0) : 'Z';

    in.close();
    switch(fmt) {
      case 'A':
        try {
          int number = new Integer(numeric);
          printASCIIFromDecimal(number - 1);
          printASCIIFromDecimal(number);
          printASCIIFromDecimal(number + 1);
        }
        catch(Exception e) {
          System.out.println("PASS");
        }
        break;
      case 'B':
        System.out.println(binaryToDemical(numeric));
        break;
      case 'H':
        System.out.println(hexToBinary(numeric));
        break;
      case 'M':
        System.out.println(octalSeriesMultiply(numeric));
        break;
      default:
        System.out.println("Goodbye.");
    }
  }
  
  private static void printASCIIFromDecimal(int i){
    char printable = (char)i;
    System.out.println(printable);
  }

  private static String binaryToDemical(String s){
    String retval = "PASS";
    if(s.length() != 4 || s.matches(".*[2-9a-zA-Z\\D].*")) {
      retval = "PASS";
    }
    else {
      int j = 0;
      for(int i = 0; i < 4; i++ ) {
        j += ((s.charAt(3-i) == '1') ? (Math.pow(2,i)) : 0);
      }
      retval = String.valueOf(j);
    }
    return retval;
  }
  private static String hexToBinary(String s){
    String retval = "";
    if(s.length() != 2 || s.matches(".*[g-zG-Z\\W].*")) { 
      retval = "PASS";
    }
    else{
      char[] carray = s.toCharArray();
      for(char c: carray) {
        switch(c) {
          case '0':
            retval += "0000";
            break;
          case '1':
            retval += "0001";
            break;
          case '2':
            retval += "0010";
            break;
          case '3':
            retval += "0011";
            break;
          case '4':
            retval += "0100";
            break;
          case '5':
            retval += "0101";
            break;
          case '6':
            retval += "0110";
            break;
          case '7':
            retval += "0111";
            break;
          case '8':
            retval += "1000";
            break;
          case '9':
            retval += "1001";
            break;
          case 'A':
            retval += "1010";
            break;
          case 'B':
            retval += "1011";
            break;
          case 'C':
            retval += "1100";
            break;
          case 'D':
            retval += "1101";
            break;
          case 'E':
            retval += "1110";
            break;
          case 'F':
            retval += "1111";
            break;
        }
      }
    }
    return retval;
  }

  private static String octalSeriesMultiply(String s){
    String retval = "";
    if(s.length() > 3 || s.matches(".*[89\\D].*")) {
      retval = "PASS";
    }
    else {
      char[] carray = s.toCharArray();
      int j = 1;
      for(char c: carray) {
        j *= Character.getNumericValue(c);
      }
      retval = String.valueOf(j);
    }
    return retval;
  }
}
