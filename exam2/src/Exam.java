import java.util.Scanner;

/**
 * @author lmc2713
 * @author Liam Carter-Condon
 * @email lmc2713@email.vccs.edu
 *
 */
public class Exam {

	/**
	 * @param args none
	 */
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Please enter two base-4 Martian digits.");
		System.out.println("Martian numbers are prefixed with 'm'.");
		System.out.println("Negative digits are denoted by a '-' after the prefix");
		String mDigitOne = in.nextLine();
		String mDigitTwo = in.nextLine();
		in.close();
		int firstDigitDecimal = mDigitStringToInt(mDigitOne);
		int secondDigitDecimal = mDigitStringToInt(mDigitTwo);
		System.out.println("Sum of Martian digits in decimal = " + (firstDigitDecimal + secondDigitDecimal));
		// Display the range of a three-bit two's complement number
		// I don't consider these magic numbers as they are part of the definition of the givens
		// I would personally consider saying "-4" and "3" magic numbers, but that's just my op.
		System.out.println(	"The range for a three-bit two's complement number is: " + 
							(int)(-1 * Math.pow(2, (3-1))) + " to " + (int)(Math.pow(2, (3-1))-1));
		String firstDigitTwosComplement = "";
		String secondDigitTwosComplement = "";
		try {
			firstDigitTwosComplement = decimalToTwosComplementBitString(firstDigitDecimal, 3);
			secondDigitTwosComplement = decimalToTwosComplementBitString(secondDigitDecimal, 3);
			System.out.println("First digit in Two's Complement \t\t= " + firstDigitTwosComplement);
			System.out.println("Second digit in Two's Complement \t\t= " + secondDigitTwosComplement);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Sum of BitStrings in Two's Complement \t\t= " + addTwosComplementBitString(firstDigitTwosComplement,secondDigitTwosComplement));
		// TODO: carry in and out from last step
		if( firstDigitDecimal + secondDigitDecimal > Math.pow(2, 3-1) - 1 || firstDigitDecimal + secondDigitDecimal < -1 * Math.pow(2, 3-1)) {
			System.out.println("Summed Bit String value is not valid (out of range)");
		}
		else System.out.println("Summed Bit String value is valid (in range)");
		
		
		System.out.println("\n\n\nA user's experience using a computer without an operating system would be very lacking - "
				+ " the motherboard would get as far as the BIOS but would not have anything to boot into (except maybe a bootloader).\n "
				+ " In general the user would not be able to do anything except interact with the BIOS - \n "
				+ "	unless they happened to be an extremely gifted programmer, in which case\n they would have to essentially write an OS\n"
				+ " = drivers for keyboard, mouse, monitor, et. all; abstraction for the disk drives to either block level IO devices or\n"
				+ " maybe straight filesystems, memory management system, system init (starting processes for all of the above), etc.");
	}
	
	public static int mDigitStringToInt(String input){
		int retval = 0;
		// test validity of input string
		if((input.charAt(0) != 'm') || Integer.parseInt(input.substring(input.length() - 1 )) > 3 || input.length() > 3 ) {
			System.out.println("This is not a valid input, please run the program again");
			System.exit(1);
		}
		else retval = Integer.valueOf(input.substring(1));
		return retval;
	}
	public static String decimalToTwosComplementBitString(int input, int bits) throws Exception {
		String retval = "";
		// I understand this is unnecessary for the exam (max input -3 -> 3)
		// This is a general base 10 to two's complement bitstring
		if (input > (Math.pow(2,bits - 1) - 1)  || input < (-1 * Math.pow(2,bits - 1))) {
			final class InputOutOfBoundsException extends Exception {
				private static final long serialVersionUID = 6225172241598761850L;
				public InputOutOfBoundsException(String string) {
					super(string);
				}				
			}
			throw new InputOutOfBoundsException("Input larger than output bitstring length");
		}
		else if (input > 0 ) {
			// easy for positive inputs
			retval = Integer.toBinaryString(input);
			while(retval.length() < bits ) {
				retval = "0" + retval;
			}
		}
		else if (input < 0 ){
			retval = Integer.toBinaryString(Math.abs(input) - 1);
			while(retval.length() < bits) {
				retval = "0" + retval;
			}
			String temp = "";
			char[] carray = retval.toCharArray();
			for(char c : carray) {
				temp += (c == '1'? "0" : "1");
			}
			retval = temp;
		}
		else retval = "000";
		return retval;
	}
	public static String addTwosComplementBitString(String a, String b) {
		String retval = "";
		boolean carry = false;
		for(int i = a.length() - 1; i >= 0; i--) {
			int t = 0;
			t += (a.charAt(i) == '1' ? 1 : 0) + (b.charAt(i) == '1' ? 1 : 0) + (carry ? 1 : 0);
			if(t >= 2 ) carry = true; else carry = false;
			retval = ((t%2 == 1) ? '1' : '0') + retval;
		}
		return retval;
	}
}