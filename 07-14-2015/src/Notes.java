import java.util.Arrays;
import java.util.Scanner;
import java.util.List;
/**
 * 
 */

/**
 * @author Liam Carter-Condon
 *
 */
public class Notes{
	
	// working on for loops for the "first" time
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Integer[] num = new Integer[3];
		for(int i = 0; i < num.length; i++)
			num[i] = in.nextInt();
		
		for(int i = 0; i < num.length; i++) 
			System.out.println("num[" + i + "] = " + num[i]);
		
		System.out.print("\nAverage of array = ");
		
		List<Integer> numbers = Arrays.asList(num);
		float average = 0;
		
		for(int i : num) average+=i;
		average /= num.length;
		System.out.println(average);

		
	}

}
