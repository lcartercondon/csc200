import java.util.Scanner;

/**
 * @author Liam Carter-Condon
 * @date 2015-07-21
 * @email lmc2713@email.vccs.edu
 */


public class HomeworkFive {

	/**
	 * @param args = none
	 */
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("I will calculate the coefficients for the expansion of (A+B) to the power of n.");
		
		System.out.println("What are the coefficients of A and B?");
		int one = in.nextInt();
		int two = in.nextInt();
		
		System.out.println("What is the power of n?");
		int power = in.nextInt();
		in.close();
		IntegerBinomial b = new IntegerBinomial(one, two, power);
		
		System.out.print("The coefficients are:" );
		for(int i = 0; i < b.getPower() + 1; i++ ) {
			System.out.print(" " + b.getTerm(i));
		}
		System.out.println(".");		
	}
}