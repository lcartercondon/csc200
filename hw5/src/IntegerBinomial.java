import java.util.ArrayList;

/**
 * 
 * @author Liam Carter-Condon
 *
 */

public class IntegerBinomial {
	private int xCoefficient, yCoefficient, power, changes;
	private ArrayList<Long> expandedCoefficients;
	
	// CONSTRUCTORS
	public IntegerBinomial() {
		xCoefficient = 1;
		yCoefficient = 1;
		power = 1;
		expandedCoefficients = new ArrayList<Long>();
		expandedCoefficients.add(1l);
		expandedCoefficients.add(1l);
	}
	public IntegerBinomial(int a, int b, int p) {
		xCoefficient = a;
		yCoefficient = b;
		power = p;
		expandedCoefficients = new ArrayList<Long>();
		generateExpandedCoefficients();
	}
	
	// PUBLIC FUNCTIONS
	public long getTerm(int j) { 
		if(changes != 0) {
			generateExpandedCoefficients();
		}
		return expandedCoefficients.get(j);
	}
	
	// PRIVATE FUNCTIONS
	private int factorial(int j) {
		if(j < 0 ) return -1; 									// do this later
		else if(j == 1 || j == 0) return 1;						// base case
		return j * factorial(j-1); 								// recursive case
	}
	
	private synchronized void generateExpandedCoefficients() { 
		expandedCoefficients.clear();
		for(int i = 0; i < power + 1; i ++)
			expandedCoefficients.add( (long) ( 
									  (factorial(power) / (factorial(i) * factorial(power - i))) *  // standard binomial term
									   Math.pow(xCoefficient,  power - i) * 						// xCoefficient term 
									   Math.pow(yCoefficient,  i)));	 							// yCoefficient term
		this.changes = 0;
	}
	
	// Getters and setters
	public int getPower() {
		return power;
	}
	
	public void setPower(int power) {
		this.power = power;
		this.changes++;
	}

	public int getxCoefficient() {
		return xCoefficient;
	}

	public void setxCoefficient(int xCoefficient) {
		this.xCoefficient = xCoefficient;
		this.changes++;
	}

	public int getyCoefficient() {
		return yCoefficient;
	}

	public void setyCoefficient(int yCoefficient) {
		this.yCoefficient = yCoefficient;
		this.changes++;
	}
}
